#!/usr/bin/python
# -*- coding: utf-8 -*-

# import module snippets
from ansible.module_utils.basic import AnsibleModule
#import subprocess
import os
import re

ANSIBLE_METADATA = {
    'status': ['preview'],
    'supported_by': 'community',
    'metadata_version': '0.1',
    'version': '0.1'
}

DOCUMENTATION = '''
---
module: slurm_facts
version_added: "0.1"
description: Gather facts for slurm services
short_description: Gather facts for slurm services

options:
author:
    - J.C. Haessig
'''

EXAMPLES = '''
'''

# =============================================
# Slurm data scraper
#

service_type={'controller':{'binary':'/usr/sbin/slurmctld','varname':'slurm_slurmctld_version','conffile':'slurm.conf'},
              'accounting':{'binary':'/usr/sbin/slurmdbd','varname':'slurm_slurmdbd_version','conffile':'slurmdbd.conf'},
              'node':{'binary':'/usr/sbin/slurmd','varname':'slurm_slurmd_version','conffile':'slurm.conf'},
              'client':{'binary':'/usr/bin/srun','varname':'slurm_srun_version','conffile':'slurm.conf'},
}



def main():

    module = AnsibleModule(
        argument_spec=dict(
        service=dict(required=True,choices=['node', 'accounting','controller','client']),
        slurm_config_dir=dict(required=True),
        ),
        supports_check_mode=True
    )
    service         = module.params["service"]
    binary=service_type[service]['binary']
    varname=service_type[service]['varname']
    slurm_config_dir=module.params["slurm_config_dir"]
    conflist={'slurm_slurmctldpidfile':{'confentry':'SlurmctldPidFile'},
              'slurm_slurmdpidfile':{'confentry':'SlurmdPidFile'},
              'slurm_slurmctldlogfile':{'confentry':'SlurmctldLogFile'},
              'slurm_slurmdlogfile':{'confentry':'SlurmdLogFile'},
              'slurm_slurmdspooldir':{'confentry':'SlurmdSpoolDir'},
              'slurm_selecttypeparameters':{'confentry':'SelectTypeParameters'},
              'slurm_slurmdbdpidfile':{'confentry':'PidFile'},
              'slurm_slurmdbdlogfile':{'confentry':'LogFile'},
    }
    resultlist={}
    has_changed = False
#    module.run_command(['/usr/sbin/slurmd','-V'], check_rc=False, close_fds=True, executable=None, data=None, binary_data=False, path_prefix=None, cwd=None, use_unsafe_shell=False, prompt_regex=None, environ_update=None, umask=None, encoding='utf-8', errors='surrogate_or_strict', expand_user_and_vars=True, pass_fds=None, before_communicate_callback=None, ignore_invalid_cwd=True, handle_exceptions=True)
    version=None
    if os.path.isfile(binary):
      result=module.run_command([binary,'-V'])
      versionparts=result[1].split(' ')
      if len(versionparts) > 1 :
        version=versionparts[1].strip()
      else:
        version=''
        resultlist['error-check-slurm-'+service]='Output ((( %s ))) Error ((( %s )))'  % (result[1],result[2])
    else:
      version=''
      resultlist['error-slurm-'+service]='not (yet) installed or executable location has changed' 

    conffile=os.path.join(slurm_config_dir,service_type[service]['conffile'])
    if os.path.isfile(conffile):
      for i, line in enumerate(open(conffile)):
        for c in conflist:
          pattern = re.compile('^'+conflist[c]['confentry']+'=(.*)') #TODO replace .* with [^ #]* ?
          for match in re.finditer(pattern, line):
            resultlist[c]=match.group(1)
            break

    resultlist[varname]=version
    module.exit_json(ansible_facts=resultlist)


if __name__ == '__main__':
    main()
