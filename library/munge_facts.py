#!/usr/bin/python
# -*- coding: utf-8 -*-

# import module snippets
from ansible.module_utils.basic import AnsibleModule
#import subprocess
import os
import re
import base64

ANSIBLE_METADATA = {
    'status': ['preview'],
    'supported_by': 'community',
    'metadata_version': '0.1',
    'version': '0.1'
}

DOCUMENTATION = '''
---
module: munge_facts
version_added: "0.1"
description: Gather facts for slurm services
short_description: Gather facts for slurm services

options:
author:
    - J.C. Haessig
'''

EXAMPLES = '''
'''

# =============================================
# Munge data scraper
#

def main():

    module = AnsibleModule(
        argument_spec=dict(
        munge_keyfile=dict(required=True),
        ),
        supports_check_mode=True
    )
    munge_keyfile=module.params["munge_keyfile"]
    resultlist={}
    has_changed = False
    #result=module.run_command([binary,'-V'])
    #versionparts=result[1].split(' ')
    #version=None
    #if len(versionparts) > 1 :
    #  version=versionparts[1].strip()
    #else:
    #  version=''
    #  resultlist['error-check-slurm-'+service]='Output ((( %s ))) Error ((( %s )))'  % (result[1],result[2])

    #keyfile=os.path.join(slurm_config_dir,munge_keyfile)
    keyfile=munge_keyfile
    if os.path.isfile(keyfile):
      resultlist['slurm_munge_key']=base64.b64encode(open(keyfile,'rb').read())
    else:
      resultlist['slurm-munge-error']="munge not (yet) installed or key location changed"
      resultlist['slurm_munge_key']=""

    module.exit_json(ansible_facts=resultlist)


if __name__ == '__main__':
    main()
